import { movieRef, userRef } from '../firebase';
import { getAuth, setAuth } from '../utils/storage';
import { isEmpty } from 'lodash';

export const saveYTUrlAction = async (dto) => {
  const { email } = getAuth();
  return new Promise((resolve, reject) => {
    const insertData = movieRef.push({ email, ...dto });
    const insertedKey = insertData.getKey();
    return resolve({ key: insertedKey, email });
  });
};

export const fetchAllMovies = async (onlyMe = false) => {
  return new Promise((resolve, reject) => {
    const cb = (snapshot) => {
      const snapshotValue = snapshot.val() || {};
      return resolve(snapshotValue);
    };
    
    if (onlyMe) {
      const { email } = getAuth();
      movieRef.orderByChild('email').equalTo(email).on('value', cb);
    } else {
      movieRef.on('value', cb);
    }
  });
};

export const checkUrlBeforShare = () => {
  const { email } = getAuth();
  
  movieRef.orderByChild('email').equalTo(email).once('value')
    .then(function (dataSnapshot) {
      // handle read data.
    });
  
};