// https://www.googleapis.com/youtube/v3/videos?part=snippet&id=dQc9WfcDPaY&key=AIzaSyCmPml0G_7m0w8fRtD-ZpclhjIBd-kqbwU

import { GlobalConfig } from '../constants';
import Axios from "axios";

const getVideoId = (url) => {
  const matches = url.match('v=(.+)');
  return matches ? matches[1] : '';
  
};
export const getVideoDetailFromYoutube = (url) => {
  const youtubeApiKey = GlobalConfig.YOUTUBE_API_KEY;
  const videoId = getVideoId(url);
  let ytApiUrl = `https://www.googleapis.com/youtube/v3/videos?part=snippet&id=${videoId}&key=${youtubeApiKey}`;
  return Axios.get(ytApiUrl)
    .then((res) => {
      console.log(res.data);
      return res.data
    })
    .catch((error) => {
      console.error(error);
      return Promise.reject(error);
    });
  // fetch(ytApiUrl,
  //   {
  //     mode: 'no-cors' // 'cors' by default
  //   }).then(resp => resp.json()).then(
  //   data => {
  //     console.log(data);
  //   }
  // );
};