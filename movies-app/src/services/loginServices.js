import { userRef } from '../firebase';
import { setAuth } from '../utils/storage';
import {isEmpty} from 'lodash';

export const loginAction = async ({ email, password }) => {
  return new Promise((resolve, reject) => {
    userRef.orderByChild('email').equalTo(email).on('value', (snapshot) => {
      const snapshotValue = snapshot.val() || {};
      if(!isEmpty(snapshotValue)) {
        snapshot.forEach(function (data) {
          const key = data.key;
          const userFind = snapshotValue[key];
          if (userFind) {
            if (userFind['password'] === password) {
              setAuth({ email, accessKey: key });
              return resolve({ logged: true });
            }
            return resolve({ logged: false, err: 'Wrong password' });
          }
        });
      } else {
        const insertData = userRef.push({ email, password });
        const insertedKey = insertData.getKey();
        setAuth({ email, accessKey: insertedKey });
        return resolve({ logged: true });
      }
    });
  });
};