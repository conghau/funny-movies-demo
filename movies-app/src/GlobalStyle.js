import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    width: 100%;
    height: 100%;
    background: #fbfbfb;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: 'Muli', sans-serif;
    font-size: 1rem;
  }

  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }
`;

export default GlobalStyle;
