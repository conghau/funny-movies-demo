import { getAuth } from './storage';

export const isAuthenticated = () => {
  const auth = getAuth();
  if (!auth) {
    return false;
  }
  
  return !!auth;
};
