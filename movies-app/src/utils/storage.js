const KEY = 'funny_movie.auth';

export const setAuth = auth => localStorage.setItem(KEY, JSON.stringify(auth));

export const getAuth = () => {
  try {
    const authData = JSON.parse(localStorage.getItem(KEY));
    return authData && authData.accessKey ? authData : null;
  } catch (error) {
    console.error('Oops! Fail to retrieve auth data from local storage', error); // eslint-disable-line
    return null;
  }
};
export const removeAuth = () => localStorage.removeItem(KEY);
