const primaryColor = '#d14242';
const primaryDarkColor = '#9c2c23';
const tertiaryColor = '#999999';
const muteColor = '#dadada';
const whiteColor = '#ffffff';
const lightWhiteColor = '#fafafa';
const blackColor = '#000000';
const lightRedColor = '#d22222';
const disabledColor = '#f2f2f2';
const disabledTextColor = '#828181';
const lightGrey = '#f9f9f9';

export default {
  breakpoints: {
    sm: '576px',
    md: '768px',
    lg: '992px',
    xl: '1200px',
  },
  lightGrey,
  blackColor,
  whiteColor,
  lightWhiteColor,
  primaryColor,
  primaryDarkColor,
  tertiaryColor,
  muteColor,
  disabledColor,
  disabledTextColor,
  lightRedColor,
  textColor: blackColor,
  textInput: {
    placeHolderColor: "#1b2733",
    disabledColor: disabledColor,
  }
};
