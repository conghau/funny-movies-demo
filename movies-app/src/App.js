import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch, } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import NotFound from './pages/NotFound';
import Movie from './pages/Movies';
import theme from './theme';
import MainLayout from './components/MainLayout';
import GlobalStyle from './GlobalStyle';
import { AuthorizedRoute } from './AuthorizedRoute';
import ShareMovie from './pages/ShareMovie';

class App extends Component {
  render () {
    return (
      <ThemeProvider theme={theme}>
        <GlobalStyle/>
        <Router basename={'/'}>
          <MainLayout>
            <Switch>
              <Route exact path="/" component={Movie}/>
              <AuthorizedRoute exact path="/me" component={Movie} onlyMe/>
              <AuthorizedRoute exact path="/me/share" component={ShareMovie}/>
              <Route component={NotFound}/>
            </Switch>
          </MainLayout>
        </Router>
      </ThemeProvider>
    );
  }
}

export default App;
