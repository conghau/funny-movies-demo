import React from 'react';
import styled from 'styled-components';
import TextInput from '../../components/TextInput';
import Button from '../../components/Button';
import { saveYTUrlAction } from '../../services/movieSevices';
import { getVideoDetailFromYoutube } from '../../services/youtube';
import { get, isEmpty } from 'lodash';
import { withRouter } from 'react-router-dom';

class ShareMovie extends React.Component {
  constructor (props) {
    super(props);
    
    this.state = {
      url: '',
      videoData: {},
      err: '',
      shared: false,
    };
  }
  
  handleBlur = (e) => {
    this.setState({ url: e.target.value, videoData: {}, err: '', shared: false });
  };
  
  gotoMyMovie = () => {
    const { history } = this.props;
    history.push('/me');
  };
  
  handleShare = async () => {
    const { url } = this.state;
    this.setState({ fetching: true, videoData: {}, err: '', shared: false });
    const videoDataResp = await getVideoDetailFromYoutube(url);
    
    const videoData = get(videoDataResp, 'items.0.snippet', {});
    
    if (!isEmpty(videoData)) {
      const { description, title, thumbnails } = videoData;
      const pictureThumb = get(thumbnails, 'default.url', '');
      
      const dto = {
        description,
        title,
        pictureThumb,
        url,
      };
      await saveYTUrlAction(dto);
      this.setState({ fetching: false, videoData, shared: true });
    } else {
      this.setState({ err: 'Something went wrong!', shared: false });
    }
  };
  
  render () {
    const { url, err, videoData: { description, title, thumbnails }, fetching, shared } = this.state;
    const pictureThumb = get(thumbnails, 'default.url', '');
    return (
      <Wrapper>
        <h2>Share Movie</h2>
        <Contain>
          <Box>
            <TextInput
              placeholder={'youtube link '} label={'Youtube URL'}
              onBlur={this.handleBlur}
              defaultValue={url}
            />
            <ExampleText>Example: https://www.youtube.com/watch?v=dQc9WfcDPaY</ExampleText>
            <span>{shared && 'Share successfully'}</span>
            <Actions>
              <Button
                isActive={!!url}
                width={'200px'}
                onClick={this.handleShare}
                disabled={!url}
              >{fetching ? `Checking ....` : 'Share'}</Button>
              
              <Button
                onClick={this.gotoMyMovie}
              >My Movies</Button>
            </Actions>
            <div>
              {fetching && `Fetching ....`}
              <p>{err && err}</p>
              {!isEmpty(title) && (
                <div>
                  <div>
                    {pictureThumb && <img src={pictureThumb}/>}
                  </div>
                  <h4>{title}</h4>
                  <span>{description}</span>
                </div>
              )}
            </div>
          </Box>
        </Contain>
      </Wrapper>
    );
  }
}

export const Wrapper = styled.div``;
export const Contain = styled.div``;
export const Box = styled.div`
  background: #FFFFFF;
  border: 0.81px solid #F2F2F2;
  box-sizing: border-box;
  box-shadow: 8px 8px 24px rgba(0, 0, 0, 0.05);
  border-radius: 4px;
  padding: 30px;
`;
export const Actions = styled.div`
  margin-top: 15px;
  button + button {
    margin-left: 15px;
  }
`;

export const ExampleText = styled.div`
  font-size: 0.875rem;
  color: #999999;
`;

export default withRouter(ShareMovie);