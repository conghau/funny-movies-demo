import React from 'react';
import MovieList from '../../components/Movies';
import { fetchAllMovies } from '../../services/movieSevices';
import { EmptyState } from '../../components/EmptyState';
import { isEmpty } from 'lodash';
import { Loading } from '../../components/Loading';

class Movies extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      movies: [],
      fetching: true,
    };
  }
  
  async componentDidMount () {
    const { onlyMe } = this.props;
    let moviesMap = await fetchAllMovies(onlyMe);
    this.setState({
      movies: Object.values(moviesMap),
      fetching: false,
    });
    
  }
  
  render () {
    const { movies, fetching } = this.state;
    const { onlyMe } = this.props;
    
    if (fetching) {
      return <Loading/>;
    }
    
    if (isEmpty(movies)) {
      return <EmptyState/>;
    }
    return (
      <div>
        <h2>{onlyMe ? `My Movie` : `Movie List`}</h2>
        <MovieList movies={movies}/>
      </div>
    );
  }
}

export default Movies;