import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { isAuthenticated } from './utils/auth';
import { getAuth } from './utils/storage';

export const AuthorizedRoute = ({
                                  component: Component,
                                  location,
                                  ...rest
                                }) => {
  const isAuth = isAuthenticated();
  const auth = getAuth();
  
  if (!isAuth) {
    sessionStorage.setItem('redirectTo', window.location.href);
  }
  
  return (
    <Route
      render={(props) =>
        isAuth ? (
          <Component auth={auth} {...props} {...rest} />
        ) : (
          <Redirect
            to={{ pathname: '/', state: { from: props.location } }}
          />
        )
      }
      {...rest}
    />
  );
};
AuthorizedRoute.propTypes = {
  component: PropTypes.shape({}).isRequired,
  location: PropTypes.shape({}).isRequired,
};
