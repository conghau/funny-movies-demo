import React from 'react';
import Logo from '../Logo';
import styled from 'styled-components/macro';
import LoginForm from '../LoginForm';
import { WelcomeUser } from './WelcomeUser';
import { LogoutButton } from '../Button/LogoutButton';
import Button from '../Button';
import { withRouter } from 'react-router-dom';

class Header extends React.Component {
  handleShareMovie = () => {
    const { history } = this.props;
    history.push('/me/share');
  };
  
  handleGotoHome = () => {
    const { history } = this.props;
    history.push('/');
  };
  
  renderLoginForm = () => {
    return <LoginForm/>;
  };
  
  renderLoggedContent = (auth) => {
    return (
      <FlexCenter>
        <WelcomeUser auth={auth}/>
        <div>
          <Button onClick={this.handleShareMovie}>Share a movie</Button>
          <LogoutButton/>
        </div>
      </FlexCenter>
    );
  };
  
  render () {
    const { auth } = this.props;
    return (
      <HeaderWrapper>
        <Logo onClick={this.handleGotoHome}/>
        {auth ? this.renderLoggedContent(auth) : this.renderLoginForm()}
      </HeaderWrapper>
    );
  }
}

const HeaderWrapper = styled.div`
  height: 60px;
  border-bottom: 1px solid;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 20px;
`;

const FlexCenter = styled.div`
  display: flex;
  align-items: center;
`;

export default withRouter(Header);