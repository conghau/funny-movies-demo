import React from 'react';

export  const WelcomeUser = ({ auth }) => {
  const { email } = auth || {};
  return (
    <p>Welcome, {email}!</p>
  );
};