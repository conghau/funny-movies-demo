import React from 'react';
import styled from 'styled-components';

export const Loading = () => {
  return (
    <Wrapper>
      <i className={'fa fa-spinner fa-spin'}></i>
    </Wrapper>
  );
};

const Wrapper = styled.div`
font-size: 3rem;
text-align: center;
color: #d47272;
`;