import styled from 'styled-components/macro';

const Button = styled.button`
  cursor: pointer;
  height: ${(p) => (p.small ? 32 : 38)}px;
  border-radius: 4px;
  font-size: 90%;
  line-height: 1.52;
  letter-spacing: normal;
  outline: 0;
  text-transform:capitalize;
  -webkit-appearance: none;
  transition: all 0.2s ease;
  background-color: ${(p) => (p.isActive ? p.theme.primaryColor : p.theme.tertiaryColor)};
  color: ${(p) => (p.isActive ? p.theme.whiteColor : p.theme.blackColor)};
  ${(p) => (p.width && `width: ${p.width};`)}

  padding: 0 16px;
  &:hover {
  }

  &:disabled {
    background-color: #dbdbdb;
    color: #7a7a7aeb;
    pointer-events: none;
    border: 1px solid silver;
  }
`;

export default Button;
