import React from 'react';
import Button from './index';
import { removeAuth } from '../../utils/storage';

export const LogoutButton = () => {
  const handleLogout = () => {
    removeAuth();
    window.location.reload();
  };
  return <Button onClick={handleLogout} isActive>Logout</Button>;
};