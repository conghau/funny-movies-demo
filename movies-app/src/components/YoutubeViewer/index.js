import React from 'react';
import ReactPlayer from 'react-player';

const YoutubeViewer = (props) => {
  return (
    <ReactPlayer {...props}/>
  );
};

export default YoutubeViewer;
