import React from 'react';
import styled from 'styled-components';
import EmptyImg from '../imgs/empty.svg';

export const EmptyState = (props) => {
  const { mainText, subText } = props;
  return (
    <Wrapper>
      <img src={EmptyImg}/>
      <MainText>{mainText || 'There is no data'}</MainText>
      {subText && <SubText>{subText}</SubText>}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  box-shadow: 8px 8px 24px rgba(0, 0, 0, 0.05);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const MainText = styled.h3`
  color: #9b9b9b;
`;
const SubText = styled.h4`
  color: #9b9b9b;
  font-size: 0.875rem;
`;