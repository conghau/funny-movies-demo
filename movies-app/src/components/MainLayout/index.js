import React from 'react';
import styled from 'styled-components';

import Header from '../Header';
import { getAuth } from '../../utils/storage';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
`;

const Container = styled.div`
  flex: auto;
  display: flex;
  flex-direction: row;
  overflow: auto;
  max-height: calc(100vh - 60px);
`;

const Main = styled.main`
  flex: auto;
  display: flex;
  flex-direction: column;
  min-height: 0;
  overflow-x: hidden;
  position: relative;

  @media (max-width: 820px) {
    padding: 30px;
  }
`;

const Content = styled.div`
  padding: 60px;
  flex: auto;
`;

function MainLayout ({
                       onLogoutClick,
                       children,
                       ...rest
                     }) {
  const auth = getAuth();
  console.log(auth);
  return (
    <Wrapper {...rest}>
      <Header auth={auth}/>
      <Container>
        <Main>
          <Content>{children}</Content>
        </Main>
      </Container>
    </Wrapper>
  );
}

export default MainLayout;