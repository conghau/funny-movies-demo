import React from 'react';
import styled from 'styled-components/macro';

const Logo = ({ onClick }) => {
  return (
    <Wrapper onClick={onClick}>
      <i className="fas fa-home"/>
      Funny Movies
    </Wrapper>
  );
};

const Wrapper = styled.div`
  font-size: 2rem;
  cursor: pointer;
`;

export default Logo;