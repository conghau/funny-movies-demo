import React from 'react';
import styled from 'styled-components/macro';
import { loginAction } from '../../services/loginServices';
import Button from '../Button';
import TextInput from '../TextInput';

class LoginForm extends React.Component {
  constructor (props) {
    super(props);
    
    this.state = {
      email: '',
      password: '',
      result: {},
    };
  }
  
  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ [name]: value });
  };
  
  handleCheck = async () => {
    const { email, password } = this.state;
    
    const result = await loginAction({ email, password });
    this.setState({ result });
    console.log(result);
    if (result.logged) {
      window.location.reload();
    }
  };
  
  render () {
    const { email, password, result: { err } } = this.state;
    return (
      <Wrapper>
        <div>
          <div>
            {err && <ErrMsg>{err}</ErrMsg>}
          </div>
          <Flex>
            <TextInput
              placeholder={'Email'}
              autoComplete={'off'}
              name={'email'}
              onChange={this.handleInputChange}
              value={email}
              type={'email'}
              required
            />
            <TextInput
              placeholder={'Password'}
              type={'password'}
              autoComplete={'new-password'}
              name={'password'}
              onChange={this.handleInputChange}
              value={password}
              required
            />
          </Flex>
        </div>
        <Button
          onClick={this.handleCheck}
          disabled={!(email && password)}
        >
          Login/Register
        </Button>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  height: 60px;
  border-bottom: 1px solid;
  display: flex;
  align-items: center;
`;

const ErrMsg = styled.span`
  color: ${props => props.theme.primaryColor};
  font-size: 0.875rem;
`;
const Flex = styled.div`
  display: flex;
`;
export default LoginForm;