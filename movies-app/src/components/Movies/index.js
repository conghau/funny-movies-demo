import React from 'react';
import MovieItem, { MovieItemWrapper } from './MovieItem';
import styled from 'styled-components/macro';

class MovieList extends React.Component {
  render () {
    const { movies = [] } = this.props;
    return (
      <Wrapper>
        {
          movies.map((movie, idx) => {
            return (
              <MovieItem key={idx} {...movie}/>
            );
          })
        }
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  ${MovieItemWrapper} {
    margin-bottom: 20px;
  }
`;

export default MovieList;