import React from 'react';
import YoutubeViewer from '../YoutubeViewer';
import styled from 'styled-components/macro';

class MovieItem extends React.Component {
  render () {
    const {
      url,
      description,
      title,
      email,
    } = this.props;
    
    
    return (
      <MovieItemWrapper>
        <VideoZone>
          <YoutubeViewer width={'300px'} height={'200px'} url={url}/>
        </VideoZone>
        <VideoDetail>
          <p>{title}</p>
          <Label>share by: {email}</Label>
          <Label>Description:</Label>
          <Description>{description}</Description>
        </VideoDetail>
      </MovieItemWrapper>
    );
  }
}

export const MovieItemWrapper = styled.div`
  display: flex;
`;
export const VideoZone = styled.div`
  min-width: 300px;
`;
export const VideoDetail = styled.div``;
export const Label = styled.p``;
export const Description = styled.span`
display: -webkit-box;
-webkit-line-clamp: 3;
-webkit-box-orient: vertical;
overflow: hidden;
text-overflow: ellipsis;
`;


export default MovieItem;