import styled from 'styled-components';

const Basic = styled.input`
  height: 48px;
  border-radius: 4px;
  padding: 1em;
  font-size: 1em;
  width: ${props => (props.width ? props.width : '100%')};
  background-color: ${props => props.theme.whiteColor};
  border: 1px solid ${props => props.theme.borderColor};

  &:disabled {
    background-color: ${props => props.theme.disabledColor};
  }
`;

export default Basic;
