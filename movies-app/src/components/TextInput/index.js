import React from 'react';
import styled from 'styled-components/macro';

const Label = styled.label.attrs({ className: 'label' })`
  line-height: 21px;
  font: inherit;
  font-size: 0.875rem;
  font-weight: bold;
  color: ${props => props.theme.textColor};
  padding: 6px 0;
  text-align: left;
`;

export const TextInputWrapContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  position: relative;
`;

const Input = styled.input.attrs(props =>
  props.password ? { type: 'password' } : {}
)`
  width: ${props => (props.width ? props.width : '100%')};
  height: 35px;
  background-color: ${props => props.theme.whiteColor};
  border-radius: 4px;
  border: 1px solid ${props => props.theme.textInput.borderColor};
  font: inherit;
  font-size: 0.875rem;
  cursor: text;
  padding: 16px;

  ::placeholder {
    color: ${props => props.theme.textInput.placeHolderColor};
  }

  &:disabled {
    background-color: ${props => props.theme.textInput.disabledColor};
  }
  ${props => props.style || {}}
`;

const TextInput = ({
                     className,
                     label,
                     errorMessage,
                     ...rest
                   }) => (
  <TextInputWrapContainer className={className}>
    {label && <Label>{label}</Label>}
    <Input  {...rest} />
  
  </TextInputWrapContainer>
);

export default TextInput;
