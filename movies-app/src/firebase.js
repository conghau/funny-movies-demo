import * as firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyBEF-x6Qx6ZEoe8OSoCCBkI-g_8QV4hhx4',
  authDomain: 'funny-movies-f509c.firebaseapp.com',
  databaseURL: 'https://funny-movies-f509c.firebaseio.com',
  projectId: 'funny-movies-f509c',
  storageBucket: 'asia-east2.appspot.com',
  messagingSenderId: '769591175930'
};
firebase.initializeApp(config);
export const databaseRef = firebase.database().ref();

export const userRef = databaseRef.child('users');
export const movieRef = databaseRef.child('movies');
